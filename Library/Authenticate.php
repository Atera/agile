<?PHP
$Authenticated = false;
$SessionID = @$_COOKIE["SessionID"];

if (@$SessionID && !isset($Connection))
{
    require("Connect.php");
}

if (@$Connection && @$SessionID)
{
    $SessionID = mysqli_escape_string($Connection, $SessionID);
    $result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Sessions WHERE SessionID='$SessionID'"));
    if ($result && $result["IP"] == $_SERVER["REMOTE_ADDR"])
    {
        if ($result["LastActive"] == null) $Authenticated = true;
        else
        {
            date_default_timezone_set("UTC");
            $lastactive = date_create($result["LastActive"]);
            $diff = date_diff($lastactive, date_create());
            if ($diff->days < 3) $Authenticated = true;
            
            // Update the last active time on the server.
            mysqli_query($Connection, "UPDATE Sessions SET LastActive=UTC_TIMESTAMP() WHERE SessionID='$SessionID'");
        }
        
        // Set the UserID for other files to know who the user is.
        if ($Authenticated == true)
        {
            $UserID = $result["UserID"];
            $result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Users WHERE UserID='$UserID';"));
            $Username = $result["Username"];
        }
    }
}

if ($Authenticated == false) $SessionID = null;
?>