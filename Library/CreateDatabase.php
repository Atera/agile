<?PHP
require("Connect.php");

// 末末末末末末末末末末末末末末末末末� Users 末末末末末末末末末末末末末末末末末�
// Detect whether the table already exists.
$result = mysqli_fetch_row(mysqli_query($Connection, "SHOW TABLES LIKE 'Users';"));
if (!$result)
{
    // Create the table if not.
    mysqli_query($Connection, "CREATE TABLE Users (UserID INT NOT NULL UNIQUE AUTO_INCREMENT, Username VARCHAR(32) NOT NULL UNIQUE, " .
                    "Email VARCHAR(255) UNIQUE, Blurb TEXT, PasswordHash CHAR(64) NOT NULL, Salt CHAR(8) NOT NULL, " .
                    "PRIMARY KEY (UserID), UNIQUE KEY (Username), UNIQUE KEY (Email));");
}
else echo "Found: ", $result[0], "\n";

// 末末末末末末末末末末末末末末末末末 Sessions 末末末末末末末末末末末末末末末末�
// Detect whether the table already exists.
$result = mysqli_fetch_row(mysqli_query($Connection, "SHOW TABLES LIKE 'Sessions';"));
if (!$result)
{
    // Create the table if not.
    mysqli_query($Connection, "CREATE TABLE Sessions (SessionID INT NOT NULL UNIQUE AUTO_INCREMENT, UserID INT NOT NULL, IP VARCHAR(15) NOT NULL, " .
                                "LastActive DATETIME, " .
                                "PRIMARY KEY (SessionID), FOREIGN KEY (UserID) REFERENCES Users (UserID) ON DELETE CASCADE ON UPDATE CASCADE);");
}
else echo "Found: ", $result[0], "\n";

// 末末末末末末末末末末末末末末末末末� Ideas 末末末末末末末末末末末末末末末末末�
// Detect whether the table already exists.
$result = mysqli_fetch_row(mysqli_query($Connection, "SHOW TABLES LIKE 'Ideas';"));
if (!$result)
{
    // Create the table if not.
    mysqli_query($Connection, "CREATE TABLE Ideas (IdeaID INT NOT NULL UNIQUE AUTO_INCREMENT, UserID INT NOT NULL, Title VARCHAR(255), " . 
                    "Description TEXT, Category ENUM('OTHER', 'CODING_TECHNIQUES', 'TEAM_MANAGEMENT', 'PROCESSES', 'TESTING', 'PROJECT_MANAGEMENT', 'ARCHITECTURE_DESIGN') NOT NULL, " .
                    "Published BOOLEAN NOT NULL DEFAULT FALSE, " .
                    "PRIMARY KEY (IdeaID), FOREIGN KEY (UserID) REFERENCES Users (UserID) ON DELETE CASCADE ON UPDATE CASCADE);");
}
else echo "Found: ", $result[0], "\n";

// 末末末末末末末末末末末末末末末末末 Comments 末末末末末末末末末末末末末末末末�
// Detect whether the table already exists.
$result = mysqli_fetch_row(mysqli_query($Connection, "SHOW TABLES LIKE 'Comments';"));
if (!$result)
{
    // Create the table if not.
    mysqli_query($Connection, "CREATE TABLE Comments (CommentID INT NOT NULL UNIQUE AUTO_INCREMENT, UserID INT NOT NULL, IdeaID INT NOT NULL, " .
                    "Content TEXT NOT NULL, " .
                    "PRIMARY KEY (CommentID), FOREIGN KEY (UserID) REFERENCES Users (UserID) ON DELETE CASCADE ON UPDATE CASCADE, " .
                    "FOREIGN KEY (IdeaID) REFERENCES Ideas (IdeaID) ON DELETE CASCADE ON UPDATE CASCADE);");
}
else echo "Found: ", $result[0], "\n";

mysqli_close($Connection);
?>