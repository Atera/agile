<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made
if (!@$_POST["IdeaID"])
{
    $jsonResponse["Succes"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$IdeaID = mysqli_escape_string($Connection, $_POST["IdeaID"]);
$limit = 0;
if (@$_POST["Limit"])
{
    $limit = mysqli_escape_string($Connection, $_POST["Limit"]);
    $limit = intval($limit);
}
$limit = ($limit == 0) ? "" : " LIMIT $limit";


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check if the idea actually exists.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Ideas WHERE IdeaID='$IdeaID';"));
if (!@$result["IdeaID"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Idea not found.";
}
else
{
    $query = mysqli_query($Connection, "SELECT * FROM Comments WHERE IdeaID='$IdeaID' ORDER BY CommentID DESC" . $limit . ";");
    
    // Get all the ideas
    $jsonResponse = array();
    while($result = mysqli_fetch_assoc($query))
    {
        $jsonResponse[] = $result;
    }
}


echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>