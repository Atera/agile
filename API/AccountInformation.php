<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["Lookup"] || ($_POST["Lookup"] != "Username" && $_POST["Lookup"] != "Details" && $_POST["Lookup"] != "Full"))
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Obtain user information.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Users WHERE UserID='$UserID';"));

$jsonResponse["Success"] = "TRUE";

switch ($_POST["Lookup"])
{
    case "Username":
        $jsonResponse["Username"] = $Username;
        break;
    case "Details":
        $jsonResponse["Email"] = $result["Email"];
        $jsonResponse["Blurb"] = $result["Blurb"];
    case "Full":
        $jsonResponse["Username"] = $Username;
        $jsonResponse["Email"] = $result["Email"];
        $jsonResponse["Blurb"] = $result["Blurb"];
        break;
}

echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>