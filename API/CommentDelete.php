<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["CommentID"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$CommentID = mysqli_escape_string($Connection, $_POST["CommentID"]);


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check if the comment actually exists.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Comments WHERE CommentID='$CommentID';"));
if (!@$result["CommentID"] || $result["UserID"] != $UserID)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "User does not own any matching idea.";
}
else
{
    mysqli_query($Connection, "DELETE FROM Comments WHERE CommentID='$CommentID';");
    $jsonResponse["Success"] = "TRUE";
}

echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>