<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["IdeaID"] || (!@$_POST["Name"] && !@$_POST["Description"] && !@$_POST["Published"] && !@$_POST["Category"]))
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$IdeaID = mysqli_escape_string($Connection, $_POST["IdeaID"]);
$Title = (@$_POST["Title"]) ? mysqli_escape_string($Connection, $_POST["Title"]) : null;
$Description = (@$_POST["Description"]) ? mysqli_escape_string($Connection, $_POST["Description"]) : null;
$Published = (@$_POST["Published"]) ? (($_POST["Published"] == "TRUE") ? true : false) : null;
$Category = (@$_POST["Category"]) ? mysqli_escape_string($Connection, $_POST["Category"]) : null;
if ($Category)
{
    switch ($Category)
    {
        case "Other":
            $Category = "OTHER";
            break;
        case "Coding Techniques":
            $Category = "CODING_TECHNIQUES";
            break;
        case "Processes":
            $Category = "PROCESSES";
            break;
        case "Testing":
            $Category = "TESTING";
            break;
        case "Project Management":
            $Category = "PROJECT_MANAGEMENT";
            break;
        case "Team Management":
            $Category = "TEAM_MANAGEMENT";
            break;
        case "Architecture and Design":
            $Category = "ARCHITECTURE_DESIGN";
            break;
        default:
            $jsonResponse["Success"] = "FALSE";
            $jsonResponse["Error"] = "Invalid Request";
            exit(json_encode($jsonResponse));    
    }
}


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check if the idea actually exists.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Ideas WHERE IdeaID='$IdeaID';"));
if (!@$result["IdeaID"] || $result["UserID"] != $UserID)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "User does not own any matching idea.";
}
else
{
    // Set up query.
    $query = "";
    if ($Title != null) $query .= "Title='$Title'";
    if ($Description != null && $query != "") $query .= ", ";
    if ($Description != null) $query .= "Description='$Description'";
    if ($Category != null && $query != "") $query .= ", ";
    if ($Category != null) $query .= "Category='$Category'";
    if ($Published != null && $query != "") $query .= ", ";
    if ($Published != null) $query .= "Published=$Published";
    
    mysqli_query($Connection, "UPDATE Ideas SET $query WHERE IdeaID='$IdeaID'");
    $jsonResponse["Success"] = "TRUE";
}

echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>