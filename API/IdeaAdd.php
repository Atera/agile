<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["Title"] || !@$_POST["Description"] || !@$_POST["Category"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$Title = strtolower(mysqli_escape_string($Connection, $_POST["Title"]));
$Description = mysqli_escape_string($Connection, $_POST["Description"]);
$Category = "";
switch ($_POST["Category"])
{
    case "Other":
        $Category = "OTHER";
        break;
    case "Coding Techniques":
        $Category = "CODING_TECHNIQUES";
        break;
    case "Processes":
        $Category = "PROCESSES";
        break;
    case "Testing":
        $Category = "TESTING";
        break;
    case "Project Management":
        $Category = "PROJECT_MANAGEMENT";
        break;
    case "Team Management":
        $Category = "TEAM_MANAGEMENT";
        break;
    case "Architecture and Design":
        $Category = "ARCHITECTURE_DESIGN";
        break;
    default:
        $jsonResponse["Success"] = "FALSE";
        $jsonResponse["Error"] = "Invalid Request";
        exit(json_encode($jsonResponse));    
}


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

mysqli_query($Connection, "INSERT INTO Ideas SET UserID='$UserID', Title='$Title', Description='$Description', Category='$Category';");
$result = mysqli_fetch_row(mysqli_query($Connection, "SELECT LAST_INSERT_ID();"));
$IdeaID = $result[0];

$jsonResponse["Success"] = "TRUE";
$jsonResponse["IdeaID"] = $IdeaID;
echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>