<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made
if (!@$_POST["Published"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$published = (($_POST["Published"] == "TRUE") ? true : false);
$limit = 0;
if (@$_POST["Limit"])
{
    $limit = mysqli_escape_string($Connection, $_POST["Limit"]);
    $limit = intval($limit);
}
$limit = ($limit == 0) ? "" : " LIMIT $limit";

$category = (@$_POST["Category"]) ? mysqli_escape_string($Connection, $_POST["Category"]) : "";
if ($Category)
{
    switch ($category)
    {
        case "Other":
            $category = "OTHER";
            break;
        case "Coding Techniques":
            $category = "CODING_TECHNIQUES";
            break;
        case "Processes":
            $category = "PROCESSES";
            break;
        case "Testing":
            $category = "TESTING";
            break;
        case "Project Management":
            $category = "PROJECT_MANAGEMENT";
            break;
        case "Team Management":
            $category = "TEAM_MANAGEMENT";
            break;
        case "Architecture and Design":
            $category = "ARCHITECTURE_DESIGN";
            break;
        default:
            $jsonResponse["Success"] = "FALSE";
            $jsonResponse["Error"] = "Invalid Request";
            exit(json_encode($jsonResponse));    
    }
}
if ($category != "") $category = " AND Category='$category'";


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check wich ideas return (if published = true we return all the published ideas else we return only the user ideas)
if ($published)
    $query = mysqli_query($Connection, "SELECT * FROM Ideas WHERE Published='$published'" . $category . $limit . ";");
else
    $query = mysqli_query($Connection, "SELECT * FROM Ideas WHERE UserID='$UserID'" . $category . $limit . ";");

// Get all the ideas
$jsonResponseElements = array();
while($result = mysqli_fetch_assoc($query))
{
    $jsonResponseElements[] = $result;
}

$jsonResponse["Success"] = "TRUE";
$jsonResponse["Content"] = $jsonResponseElements;

echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>