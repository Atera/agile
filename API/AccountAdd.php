<?PHP
require("../Library/Connect.php");

// Detect if a valid request was made.
if (!@$_POST["Username"] || !@$_POST["Password"] || !@$_POST["Email"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$RequestedUsername = strtolower(mysqli_escape_string($Connection, $_POST["Username"]));
$RequestedPassword = mysqli_escape_string($Connection, $_POST["Password"]);
$RequestedEmail = strtolower(mysqli_escape_string($Connection, $_POST["Email"]));

// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

if (mysqli_fetch_row(mysqli_query($Connection, "SELECT * FROM Users WHERE username='$RequestedUsername';")))
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Username Taken";
    exit(json_encode($jsonResponse));
}

// Create a new user.
$salt = RandomString(8);
$passwordhash = hash("sha256", $RequestedPassword . $salt);
$result = mysqli_query($Connection, "INSERT INTO Users SET Username='$RequestedUsername', Email='$RequestedEmail', PasswordHash='$passwordhash', salt='$salt';");

// Create the response.
$jsonResponse["Success"] = ($result) ? "TRUE" : "FALSE";
if (!$result) $jsonResponse["Error"] = "Registration failed.";
echo json_encode($jsonResponse);

@mysqli_close($Connection);

function RandomString($length)
{
    $output = "";
    $characterset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,!?@#^&*(){}[]-%_';
    for ($x = 0; $x < $length; $x++)
        $output .= substr($characterset,rand(0,79),1);
    return $output;
}
?>