<?PHP
@require("../Library/Connect.php");

// Check for a correctly formatted login attempt.
if (!@$_POST["Username"] || !@$_POST["Password"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}


$username = strtolower(mysqli_real_escape_string($Connection, $_POST["Username"]));
$password = mysqli_real_escape_string($Connection, $_POST["Password"]);


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check for valid credentials.
$logonsuccess = false;
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Users WHERE Username='$username';"));
if (@$result["PasswordHash"])
{
    $hash = hash("sha256", $password . $result["Salt"]);
    if ($hash == $result["PasswordHash"]) $logonsuccess = true;
}

if (!$logonsuccess)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Incorrect username or password.";
    exit(json_encode($jsonResponse));
}


$UserID = $result["UserID"];

// Maintenance: Remove expired sessions.
mysqli_query($Connection, "DELETE FROM Sessions WHERE UserID='$UserID' AND LastActive IS NOT NULL AND TIMEDIFF(UTC_TIMESTAMP(), LastActive) > '36:00:00';");

// Create the session.
$ip = $_SERVER["REMOTE_ADDR"]; // Obtain the IP address.
mysqli_query($Connection, "INSERT INTO Sessions SET UserID='$UserID', IP='$ip', LastActive=UTC_TIMESTAMP();");
$result = mysqli_fetch_row(mysqli_query($Connection, "SELECT LAST_INSERT_ID();"));
$SessionID = $result[0];

$jsonResponse["Success"] = "TRUE";
$jsonResponse["SessionID"] = $SessionID;
echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>