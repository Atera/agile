<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["IdeaID"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$IdeaID = mysqli_escape_string($Connection, $_POST["IdeaID"]);


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check if the idea actually exists.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Ideas WHERE IdeaID='$IdeaID';"));
if (!@$result["IdeaID"] || $result["UserID"] != $UserID)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "User does not own any matching idea.";
}
else
{
    mysqli_query($Connection, "DELETE FROM Ideas WHERE IdeaID='$IdeaID';");
    $jsonResponse["Success"] = "TRUE";
}

echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>