<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["IdeaID"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

$IdeaID = mysqli_escape_string($Connection, $_POST["IdeaID"]);


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Check if the idea actually exists.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Ideas WHERE IdeaID='$IdeaID';"));
if (!@$result["IdeaID"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Could not find idea.";
}
else
{
    $jsonResponse["Success"] = "TRUE";
    $jsonResponse["Title"] = $result["Title"];
    $jsonResponse["Description"] = $result["Description"];
    $jsonResponse["Category"] = EvaluateCategory($result["Category"]);
    
    $commentsResult = mysqli_fetch_row($query($Connection, "SELECT COUNT(*) FROM Comments WHERE IdeaID='$IdeaID';"));
    $jsonResponse["Comments"] = $commentsResult[0];
    
    if ($result["UserID"] == $UserID)
    {
        $jsonResponse["Published"] = ($result["Published"]) ? "TRUE" : "FALSE";
    }
    else
    {
        $ideaUserID = $result["UserID"];
        $userResult = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Users WHERE UserID='$ideaUserID';"));
        $jsonResponse["Author"] = $userResult["Username"];
    }
}


echo json_encode($jsonResponse);

@mysqli_close($Connection);

function EvaluateCategory($category)
{
    switch ($category)
    {
        case "OTHER":
            return "Other";
        case "CODING_TECHNIQUES":
            return "Coding Techniques";
        case "PROCESSES":
            return "Processes";
        case "TESTING":
            return "Testing";
        case "PROJECT_MANAGEMENT":
            return "Project Management";
        case "TEAM_MANAGEMENT":
            return "Team Management";
        case "ARCHITECTURE_DESIGN":
            return "Architecture and Design";
    }
}
?>