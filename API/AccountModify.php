<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["Username"] && !@$_POST["Email"] && !@$_POST["Blurb"] && !@$_POST["Password"])
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}

// Detect whether the correct password was used.
$result = mysqli_fetch_assoc(mysqli_query($Connection, "SELECT * FROM Users WHERE UserID='$UserID';"));
if (@$result["PasswordHash"])
{
    $hash = hash("sha256", $_POST["Password"] . $result["Salt"]);
    if ($hash != $result["PasswordHash"])
    {
        $jsonResponse["Success"] = "FALSE";
        $jsonResponse["Error"] = "Incorrect Password";
        exit(json_encode($jsonResponse));
    }
}

$Username = (@$_POST["Username"]) ? mysqli_escape_string($Connection, $_POST["Username"]) : null;
$Email = (@$_POST["Email"]) ? mysqli_escape_string($Connection, $_POST["Email"]) : null;
$Blurb = (@$_POST["Blurb"]) ? mysqli_escape_string($Connection, $_POST["Blurb"]) : null;
$RequestedPassword = (@$_POST["NewPassword"]) ? $_POST["NewPassword"] : null ;


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

// Set up query.
$query = "";
if ($Username != null) $query .= "Username='$Username'";
if ($Email != null && $query != "") $query .= ", ";
if ($Email != null) $query .= "Email='$Email'";
if ($Blurb != null && $query != "") $query .= ", ";
if ($Blurb != null) $query .= "Blurb='$Blurb'";

if ($RequestedPassword != null)
{
    $salt = RandomString(8);
    $passwordhash = hash("sha256", $RequestedPassword . $salt);
    if ($query != "") $query .= ", ";
    $query .= "PasswordHash='$passwordhash', salt='$salt'";
}

mysqli_query($Connection, "UPDATE Users SET $query WHERE UserID='$UserID';");
$jsonResponse["Success"] = "TRUE";

echo json_encode($jsonResponse);

@mysqli_close($Connection);

function RandomString($length)
{
    $output = "";
    $characterset = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,!?@#^&*(){}[]-%_';
    for ($x = 0; $x < $length; $x++)
        $output .= substr($characterset,rand(0,79),1);
    return $output;
}
?>