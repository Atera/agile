<?PHP
require("../Library/Connect.php");
require("../Library/Authenticate.php");

// Reject request if not authenticated.
if (!$Authenticated)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Not Authenticated";
    exit(json_encode($jsonResponse));
}

// Detect if a valid request was made.
if (!@$_POST["Username"] || $_POST["Username"] != $Username)
{
    $jsonResponse["Success"] = "FALSE";
    $jsonResponse["Error"] = "Invalid Request";
    exit(json_encode($jsonResponse));
}


// 末末末末末末末末末末末末末末末末末末末末末末� Actual Processing 末末末末末末末末末末末末末末末末末末末末末末�

mysqli_query($Connection, "DELETE FROM Users WHERE UserID='$UserID'");

// Normally not needed because the database will update itself, but some versions have problems.
mysqli_query($Connection, "DELETE FROM Sessions WHERE UserID='$UserID'");
mysqli_query($Connection, "DELETE FROM Ideas WHERE UserID='$UserID'");
mysqli_query($Connection, "DELETE FROM Comments WHERE UserID='$UserID'");


$jsonResponse["Success"] = "TRUE";
echo json_encode($jsonResponse);

@mysqli_close($Connection);
?>