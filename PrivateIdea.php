<?PHP
include("Library/Authenticate.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="./bootstrap-3.3.0/favicon.ico">
<title>IdeaXchange</title>

<!-- Bootstrap core CSS -->
<link href="./Bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="./Bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./CSS/theme.css" rel="stylesheet">
</head>
<body role="document">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="Index.php">IdeaXchange</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
		<li><a href="Browse.php">Browse</a></li>
		<li><a id="MyIdeasNavLink" href="<?PHP if ($Authenticated) echo "MyIdeas.php"; else echo "Login.php?Page=MyIdeas.php"; ?>">My Ideas</a></li>
		<li><a id="MyAccountNavLink" href="<?PHP if ($Authenticated) echo "MyAccount.php"; else echo "Login.php?Page=MyAccount.php"; ?>">My Account</a></li>
		</ul>
		
        <?PHP
        if (!$Authenticated)
        {
            ?>
            <form id="NavigationLogin" class="navbar-form navbar-right" role="form">
		    <div class="form-group">
		    <input id="LoginUsername" type="text" placeholder="Username" class="form-control">
		    </div>
		    <div class="form-group">
		    <input id="LoginPassword" type="password" placeholder="Password" class="form-control">
		    </div>
		    <button id="LoginSubmit" class="btn btn-Primary">Log in</button>
		    <button class="btn btn-success" onclick="javascript: document.location.pathname = '/Signup.html'; return false;">Sign up</button>
            </form>
            <?PHP
        }
        else echo "<div class='navbar-right'>Welcome, ", $Username, "! <button onclick='javascript: document.cookie=\"SessionID=0\"; location.reload();'>Logout</button></div>";
        ?>
		
	</div>
	</div>
	</nav>
    <!-- End copy-pastable material -->

  <div class="container theme-showcase" role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron ">
      <div class="container">
        <h2 style="text-align : center;"> Edit My Idea</h2>
      </div>
      <form class="form-horizontal" role="form">
        <div class="form-group">
          <!-- I can't do change "Edit" to "Save" here, so I just put icon here-->
          <a herf="#" class="col-md-offset-9 btn-sm glyphicon glyphicon-pencil" role="button" button id="EditAccount" aria-hidden="true"></a>
          <a herf="#" class="glyphicon btn-sm glyphicon-floppy-disk" role="button" button id="SaveChange" aria-hidden="true"></a>
          <a herf="#" class="glyphicon btn-sm glyphicon-remove" role="button" button id="Cancel" aria-hidden="true"></a>
        </div>
        <div class="form-group">
          <label for="LabelinputTitle" class="col-sm-3 control-label">Title</label>
          <div class="col-sm-7">
            <input type="Text" class="col-sm-3 form-control" id="inputTitle" placeholder="Title"></input>
          </div>
        </div>
        <div class="form-group">
          <label for="LabelinputIdeaContent" class="col-sm-3 control-label">Content</label>
          <div class="col-sm-7">
            <textarea type="Text" rows="20" class="col-sm-3 form-control"  id="inputIdeaContent" placeholder="Fill in the content of your idea here."></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-offset-6">
          <input type="Checkbox" id="CheckboxIsPublish">  Publish</input>
        </div>
      </div>
      <div class="form-group">
          <div class="col-sm-offset-4 col-sm-8">
            <a href="#" class="col-sm-3 btn btn-sm btn-danger" role="button" button id="DeleteIdea">Delete This Idea</a>
            <a href="#" class="col-sm-offset-1 col-sm-3 btn btn-sm btn-primary" role="button" button id="ViewPublish">View Publish</a>
          </div>
        </div>
      </form>
    </div>

<!-- Placed at the end of the document so the pages load faster -->

	<script src="./JavaScript/jquery-2.1.1.js"></script>

	<script src="./Bootstrap/js/bootstrap.min.js"></script>

	<script src="./JavaScript/Login.js"></script>
	
	<script src="./JavaScript/PrivateIdea.js"></script>

</body>

</html>

