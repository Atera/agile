<?PHP
include("Library/Authenticate.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="./bootstrap-3.3.0/favicon.ico">
<title>IdeaXchange</title>

<!-- Bootstrap core CSS -->
<link href="./Bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="./Bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./CSS/theme.css" rel="stylesheet">
</head>
<body role="document">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="Index.php">IdeaXchange</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
		<li><a href="Browse.php">Browse</a></li>
		<li><a id="MyIdeasNavLink" href="<?PHP if ($Authenticated) echo "MyIdeas.php"; else echo "Login.php?Page=MyIdeas.php"; ?>">My Ideas</a></li>
		<li><a id="MyAccountNavLink" href="<?PHP if ($Authenticated) echo "MyAccount.php"; else echo "Login.php?Page=MyAccount.php"; ?>">My Account</a></li>
		</ul>
		
        <?PHP
        if (!$Authenticated)
        {
            ?>
            <form id="NavigationLogin" class="navbar-form navbar-right" role="form">
		    <div class="form-group">
		    <input id="LoginUsername" type="text" placeholder="Username" class="form-control">
		    </div>
		    <div class="form-group">
		    <input id="LoginPassword" type="password" placeholder="Password" class="form-control">
		    </div>
		    <button id="LoginSubmit" class="btn btn-Primary">Log in</button>
		    <button class="btn btn-success" onclick="javascript: document.location.pathname = '/Signup.html'; return false;">Sign up</button>
            </form>
            <?PHP
        }
        else echo "<div class='navbar-right'>Welcome, ", $Username, "! <button onclick='javascript: document.cookie=\"SessionID=0\"; location.reload();'>Logout</button></div>";
        ?>
		
	</div>
	</div>
	</nav>
    <!-- End copy-pastable material -->
    
  <div class="container theme-showcase" role="main">
  <div class="jumbotron">
    <p>
    <form class="form-horizontal" role="form">
      <div class="form-group">
        <label for="inputUsername3" class="col-sm-2 control-label">Username</label>
        <div class="col-sm-5">
          <input type="Text" class="form-control" id="inputUsername" placeholder="Username">
        </div>
      </div>
      <div class="form-group">
        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
        <div class="col-sm-5">
          <input type="Password" class="form-control" id="inputPassword" placeholder="Password">
        </div>
      </div>
      <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">

          <a href="#" class="col-sm-2 btn btn-lg btn-primary" role="button" button id="LogIn">Log In</a>

          <a href="#" class="col-sm-offset-1 col-sm-2 btn btn-lg btn-success" role="button" button id="SignUp">Sign Up</a>
        </div>
      </div>
    </form>
  </p>
</div>
</div>

  <div class="container theme-showcase" role="main">

   	    
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="./JavaScript/jquery-2.1.1.js"></script>
	<script src="./Bootstrap/js/bootstrap.min.js"></script>
	<script src="./JavaScript/Login.js"></script>
</body>
</html>
