<?PHP
include("Library/Authenticate.php");
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="./bootstrap-3.3.0/favicon.ico">
<title>IdeaXchange</title>

<!-- Bootstrap core CSS -->
<link href="./Bootstrap/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="./Bootstrap/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="./CSS/theme.css" rel="stylesheet">
</head>
<body role="document">
	<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
	<div class="container">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed">
		<span class="sr-only">Toggle navigation</span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="Index.php">IdeaXchange</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
		<ul class="nav navbar-nav">
		<li><a href="Browse.php">Browse</a></li>
		<li><a id="MyIdeasNavLink" href="<?PHP if ($Authenticated) echo "MyIdeas.php"; else echo "Login.php?Page=MyIdeas.php"; ?>">My Ideas</a></li>
		<li><a id="MyAccountNavLink" href="<?PHP if ($Authenticated) echo "MyAccount.php"; else echo "Login.php?Page=MyAccount.php"; ?>">My Account</a></li>
		</ul>
		
        <?PHP
        if (!$Authenticated)
        {
            ?>
            <form id="NavigationLogin" class="navbar-form navbar-right" role="form">
		    <div class="form-group">
		    <input id="LoginUsername" type="text" placeholder="Username" class="form-control">
		    </div>
		    <div class="form-group">
		    <input id="LoginPassword" type="password" placeholder="Password" class="form-control">
		    </div>
		    <button id="LoginSubmit" class="btn btn-Primary">Log in</button>
		    <button class="btn btn-success" onclick="javascript: document.location.pathname = '/Signup.html'; return false;">Sign up</button>
            </form>
            <?PHP
        }
        else echo "<div class='navbar-right'>Welcome, ", $Username, "! <button onclick='javascript: document.cookie=\"SessionID=0\"; location.reload();'>Logout</button></div>";
        ?>
		
	</div>
	</div>
	</nav>
    <!-- End copy-pastable material -->

  <div class="container theme-showcase" role="main">

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron ">
      <div class="container-fluid">
        <div class="row-fluid">
          <form class="form-horizontal" role="form">
          <div class="form-group">
            <label for="LabelSortBy" class="col-sm-5 control-label">Sort by</label>
              <div class="dropdown theme-dropdown clearfix">
                <a id="SortMenu" href="#" role="button" class="sr-only dropdown-toggle" data-toggle="dropdown">Sort By <b class="caret"></b></a>
                <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                  <li class="active" role="presentation"><a role="menuitem" tabindex="-1" href="#">Author</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Date</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Number of Comments</a></li>
              </ul>
              </div>
          </div>
        </form>
      </div>
        <div class="col-sm-12">
          <div id="IdeasContainer" class="list-group">
            <a href="#" class="list-group-item active">
              <h2 class="list-group-item-heading">An Excellent Idea</h2>
              <h4 class="list-group-item-author">Auther:Mori</h4>
              <p class="list-group-item-text">This is a good idea which deserve you to investigate. I am an academic researcher from the most
                famous university in Mihua. I recently come up with an Idea....</p>
            </a>
            <a href="#" class="list-group-item">
              <h2 class="list-group-item-heading">An Good Idea</h2>
              <h4 class="list-group-item-author">Auther:Mori</h4>
              <p class="list-group-item-text">This is a good idea which deserve you to investigate. I am an academic researcher from the most
                famous university in Mihua. I recently come up with an Idea....</p>
            </a>
            <a href="#" class="list-group-item">
              <h2 class="list-group-item-heading">A Bad Idea</h2>
              <h4 class="list-group-item-author">Auther:Johnson</h4>
              <p class="list-group-item-text">Do not invest in this idea if you don't want to waste your money. I am a software manager from the most
                famous university in Mihua. I recently come up with an Idea....</p>
            </a>
          </div>
        </div><!-- /.col-sm-4 -->
          <div class="col-sm-offset-4">
            <div class="pagination pagination-lg pagination-centered">
              <li><a href="#">&laquo;</a></li>
              <li><a href="#">1</a></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#">&raquo;</a></li>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Placed at the end of the document so the pages load faster -->
	<script src="./JavaScript/jquery-2.1.1.js"></script>
	<script src="./Bootstrap/js/bootstrap.min.js"></script>
	<script src="./JavaScript/Login.js"></script>
    <script src="./JavaScript/FetchIdeas.js"></script>
    <script src="./JavaScript/Browse.js"></script>
</body>
</html>
