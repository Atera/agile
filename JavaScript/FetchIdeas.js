﻿function FetchIdeas(published, category, limit)
{
    if (published == null || published == true) published = "TRUE";
    else published = "FALSE";

    sendInfo = null;
    if (category != null)
    {
        if (limit != null)
        {
            sendInfo = { Published: published, Category: category, Limit: limit };
        }
        else sendInfo = { Published: published, Category: category };
    }
    else if (limit != null)
        sendInfo = { Published: published, Limit: limit };
    else
        sendInfo = { Published: published };

    $.post("API/Ideas.php", sendInfo, function (data) {
        if (data.Success == "FALSE")
        {
            alert(data.Error);
            return false;
        }
        
        return "hello"; //data.Success;
    }, "json");
}