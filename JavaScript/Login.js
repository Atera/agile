﻿$(document).ready(function () {
    $("#LoginSubmit").click(function () {
        var username = $("#LoginUsername").val();
        var password = $("#LoginPassword").val();
        // Checking for blank fields.
        if (username == '' || password == '') {
            alert("Please fill all the fields");
        }
        else {
            sendinfo = { Username: username, Password: password };
            $.ajax({
                type: 'POST',
                url: 'API/Login.php',
                dataType: 'json',
                data: sendinfo,
                success: function (data) {
                    if (data.Success == "TRUE") {
                        setCookie("SessionID", data.SessionID, 3);
                        $("#NavigationLogin").html("<div class='navbar-form navbar-right'>Welcome, " + username + "!</div>");
                        $("#MyIdeasNavLink").attr("href", "MyIdeas.php");
                        $("#MyAccountNavLink").attr("href", "MyAccount.php");
                    }
                    else alert(data.Error);
                }

            });
        }

        return false;
    });
});

function setCookie(Name, Value, DaysValid) {
    var currentTime = new Date();
    currentTime.setTime(currentTime.getTime() + (DaysValid * 24 * 60 * 60 * 1000));
    document.cookie = Name + "=" + Value + "; expires=" + currentTime.toUTCString();
}