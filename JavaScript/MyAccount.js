$(document).ready(function () {
    var username, email, personalBlurb, password, newPassword, newPasswordConf;

    // Call API to get user info
    sendinfo = { Lookup: "Full" };
    $.ajax({
        type: 'POST',
        url: 'API/AccountInformation.php',
        dataType: 'json',
        data: sendinfo,
        success: function (data) {
            if (data.Success == "TRUE") {
                username = data.Username;
                email = data.Email;
                personalBlurb = data.Blurb;

                $("#inputUsername").val(username);
                $("#inputUsername").attr("readonly", true);

                $("#inputOldPassword").val();
                $("#inputOldPassword").attr("readonly", true);

                $("#inputNewPassword").val();
                $("#inputNewPassword").attr("readonly", true);

                $("#inputConfPassword").val();
                $("#inputConfPassword").attr("readonly", true);

                $("#inputEmail").val(email);
                $("#inputEmail").attr("readonly", true);

                $("#inputPersonalBlurb").val(personalBlurb);
                $("#inputPersonalBlurb").attr("readonly", true);
            }
            else alert(data.Error);
        }

    });

    //Show edit button
    $("#EditAccount").show();
    $("#SaveChange").hide();
    $("#Cancel").hide();


    //Dislplay the info
    $("#EditAccount").click(function () {
        // Make all the inputs editable
        $("#inputUsername").attr("readonly", false);
        $("#inputEmail").attr("readonly", false);
        $("#inputPersonalBlurb").attr("readonly", false);
        $("#inputOldPassword").attr("readonly", false);
        $("#inputNewPassword").attr("readonly", false);
        $("#inputConfPassword").attr("readonly", false);

        // Make the buttons visible/invisible
        $("#SaveChange").show();
        $("#Cancel").show();
        $("#DeleteAccount").hide();
        $("#EditAccount").hide();
    });

    $("#Cancel").click(function () {
        //make all the inputs  not editable

        $("#inputUsername").attr("readonly", true);
        $("#inputPassword").attr("readonly", true);
        $("#inputEmail").attr("readonly", true);
        $("#inputPersonalBlurb").attr("readonly", true);
        $("#inputOldPassword").attr("readonly", true);
        $("#inputNewPassword").attr("readonly", true);
        $("#inputConfPassword").attr("readonly", true);

        //get back the prevoius data
        $("#inputUsername").val(username);
        $("#inputEmail").val(email);
        $("#inputPersonalBlurb").val(personalBlurb);

        //make the buttons visible/invisible
        $("#SaveChange").hide();
        $("#Cancel").hide();
        $("#DeleteAccount").show();
        $("#EditAccount").show();
    });

    $("#SaveChange").click(function () {
        // Call the API to insert the new data
        username = $("#inputUsername").val();
        email = $("#inputEmail").val();
        personalBlurb = $("#inputPersonalBlurb").val();
        password = $("#inputOldPassword").val();
        newPassword = $("#inputNewPassword").val();
        newPasswordConf = $("#inputConfPassword").val();


        if (password == '' || username == '' || email == '') {
            alert("Please fill all the mandatory fields");
        } else {
            if (newPassword != "" || newPasswordConf != "") {
                
                    if (newPassword == "" || newPasswordConf == "") {
                        alert("Please enter both new password and new password confirmation");
                    } else {
                        if (newPassword != newPasswordConf) {
                            alert("The new password and the confirmation doesn't match");
                        } else {

                            var sendInfo = {
                                Username: username,
                                Email: email,
                                Blurb: personalBlurb,
                                Password: password,
                                NewPassword: newPassword
                            };

                            $.ajax({
                                type: 'POST',
                                url: 'API/AccountModify.php',
                                dataType: 'json',
                                data: sendInfo,
                                success: function (data) {
                                    if (data.Success == "TRUE") {
                                        alert("Changes saved correctly.");
                                    } else alert(data.Error);

                                    // Make the buttons visible/invisible
                                    $("#SaveChange").hide();
                                    $("#Cancel").hide();
                                    $("#EditAccount").show();
                                    $("#DeleteAccount").show();
                                    // Make all the inputs editable
                                    $("#inputUsername").attr("readonly", true);
                                    $("#inputEmail").attr("readonly", true);
                                    $("#inputPersonalBlurb").attr("readonly", true);
                                    $("#inputOldPassword").attr("readonly", true);
                                    $("#inputNewPassword").attr("readonly", true);
                                    $("#inputConfPassword").attr("readonly", true);
                                }
                            });
                        }
                    
                }
            } else {
                var sendInfo = {
                    Username: username,
                    Email: email,
                    Password: password,
                    Blurb: personalBlurb
                };

                $.ajax({
                    type: 'POST',
                    url: 'API/AccountModify.php',
                    dataType: 'json',
                    data: sendInfo,
                    success: function (data) {
                        if (data.Success == "TRUE") {
                            alert("Changes saved correctly.");
                        } else alert(data.Error);

                        // Make the buttons visible/invisible
                        $("#SaveChange").hide();
                        $("#Cancel").hide();
                        $("#DeleteAccount").show();
                        $("#EditAccount").show();
                        // Make all the inputs editable
                        $("#inputUsername").attr("readonly", true);
                        $("#inputEmail").attr("readonly", true);
                        $("#inputPersonalBlurb").attr("readonly", true);
                        $("#inputOldPassword").attr("readonly", true);
                        $("#inputNewPassword").attr("readonly", true);
                        $("#inputConfPassword").attr("readonly", true);
                    }
                });
            }
        }
    });

    $("#DeleteAccount").click(function () {
        if (confirm("Do you really want to delete the user account?") == true) {

            $.post('API/AccountDelete.php', { Username: username }, function (data) {
                if (data.Success == "TRUE") {
                    alert("Account deleted.");
                    document.location = "Index.php";
                } else alert(data.Error);
            }, "json");
        }
    });
});
