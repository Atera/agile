$(document).ready(function () {

    var title, ideaContent, author, publicId, published;
    var ideaID = getURLParameter('ideaID');

    //call API to get user info
    $.post('API/IdeaInformation.php',ideaID, function (data) {
        if (result.Succes == "TRUE") {
            title = data.Title;
            ideaContent = data.Description;
            author = data.Author;
            published = data.Published;
        } else alert(data.Error);
    });

    //Show edit button
    $("#EditAccount").show();
    $("#SaveChange").hide();
    $("#Cancel").hide();


    //Dislplay the info
    $("#inputTitle").val(title);
    $("#inputTitle").attr("readonly", true);

    $("#inputIdeaContent").val(ideaContent);
    $("#inputIdeaContent").attr("readonly", true);

    $("#CheckboxIsPublish").attr('checked', published);
    $("#CheckboxIsPublish").attr("disabled", true);


    $("#EditAccount").click(function () {
        //make all the inputs editable
        $("#inputTitle").attr("readonly", false);
        $("#inputIdeaContent").attr("readonly", false);
        $("#CheckboxIsPublish").attr("disabled", false);
        //make the buttons visible/invisible
        $("#SaveChange").show();
        $("#Cancel").show();
        $("#EditAccount").hide();

    });

    $("#Cancel").click(function () {
        //make all the inputs  not editable

        $("#inputTitle").attr("readonly", true);
        $("#inputIdeaContent").attr("readonly", true);
        $("#CheckboxIsPublish").attr("disabled", true);

        //get back the prevoius data
        $("#inputTitle").val(title);
        $("#inputIdeaContent").val(ideaContent);

        //make the buttons visible/invisible
        $("#SaveChange").hide();
        $("#Cancel").hide();
        $("#EditAccount").show();
    });

    $("#SaveChange").click(function () {
        //call the API to insert the new data
        title = $("#inputTitle").val();
        ideaContent = $("#inputIdeaContent").val();
        if ($("#CheckboxIsPublish").is(':checked')) {
            publish = "TRUE";
        } else {
            publicId = "FALSE";
        }

        if (title === "" | ideaContent === "") {
            alert("Please fill al the mandatory fields");
        } else {

            var sendInfo = {
                IdeaID: ideaId,
                Title: title,
                Description: ideaContent,
                Published: publish
            };

            $.post("API/IdeaModify.php", sendInfo, function (data) {
                if (data.Succes == "TRUE") {
                    alert("Changes saved correctly");
                } else {
                    alert(data.Error);
                }
            });

            //make the buttons visible/invisible
            $("#SaveChange").hide();
            $("#Cancel").hide();
            $("#EditAccount").show();
        }
    });

    $("#DeleteIdea").click(function () {
        // call to API deleteUser
        if (confirm("Do you really want to delete the idea " + title + " ?") == true) {
            $.post('API/IdeaDelete.php', ideaID, function (data) {
                if (data.Succes == "TRUE") {
                    alert("Account deleted.");
                    window.location = "Index.html";
                } else alert(data.Error);
            });
        }
    });

    $("#ViewPublish").click(function () {
        window.location = "PublicIdea.html?ideaID=" + ideaID;
    });

    function getURLParameter(name) {
        return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [, ""])[1].replace(/\+/g, '%20')) || null;
    }

});
