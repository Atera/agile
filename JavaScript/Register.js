﻿$(document).ready(function () {
    $("#RegistrationSubmit").click(function () {
        var username = $("#InputUsername").val();
        var password = $("#InputPassword").val();
        var repeatpassword = $("#RepeatInputPassword").val();
        var email = $("#InputEmail").val();
        // Checking for blank fields.
        if (username == '' || password == '' || repeatpassword != password || email == '') {
            alert("Please fill all the fields");
        }
        else {
            sendinfo = { Username: username, Password: password, Email: email };
            $.ajax({
                type: 'POST',
                url: 'API/AccountAdd.php',
                dataType: 'json',
                data: sendinfo,
                success: function (data) {
                    if (data.Success == "TRUE") {
                        sendinfo = { Username: username, Password: password };
                        $.ajax({
                            type: 'POST',
                            url: 'API/Login.php',
                            dataType: 'json',
                            data: sendinfo,
                            success: function (data) {
                                if (data.Success == "TRUE")
                                {
                                    setCookie("SessionID", data.SessionID, 3);
                                    document.location = "Index.php";
                                }
                                else alert(data.Error);
                            }
                        });
                    }
                    else alert(data.Error);
                }
            });
        }
    });
});

function setCookie(Name, Value, DaysValid) {
    var currentTime = new Date();
    currentTime.setTime(currentTime.getTime() + (DaysValid * 24 * 60 * 60 * 1000));
    document.cookie = Name + "=" + Value + "; expires=" + currentTime.toUTCString();
}